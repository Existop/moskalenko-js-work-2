const {City} = require('./City')
const {Country} = require('./Country')

let city1 = new City('Kharkiv')
let city2 = new City('Lviv')
let city3 = new City('Kyiv')
let city4 = new City('Ternotol')
let city5 = new City('Odessa')
let city6 = new City('Rivne')
let city7 = new City('lutsk')
let city8 = new City('Krivoy Rog')
let city9 = new City('Uzhgorod')
let city10 = new City('Dnieper')

city1.setForecast ().then(() => {console.log(city1)}).catch((error) => {console.log(error)})
city3.setForecast ().then(() => {console.log(city2)}).catch((error) => {console.log(error)})
city3.setForecast ().then(() => {console.log(city3)}).catch((error) => {console.log(error)})
city4.setForecast ().then(() => {console.log(city4)}).catch((error) => {console.log(error)})
city5.setForecast ().then(() => {console.log(city5)}).catch((error) => {console.log(error)})
city6.setForecast ().then(() => {console.log(city6)}).catch((error) => {console.log(error)})
city7.setForecast ().then(() => {console.log(city7)}).catch((error) => {console.log(error)})
city8.setForecast ().then(() => {console.log(city8)}).catch((error) => {console.log(error)})
city9.setForecast ().then(() => {console.log(city9)}).catch((error) => {console.log(error)})
city10.setForecast ().then(() => {console.log(city10)}).catch((error) => {console.log(error)})


const getForecast = (countryName) => Country[Country.findIndex(({name}) => name === countryName)].forecast || 'N/A';
getForecast('Ukraine')
getForecast('Polsha')

const setForecast = (forecast, countryName) => {
    const findedCountry = Country[Country.findIndex(({name}) => name === countryName)];
    return findedCountry.forecast = forecast;
}
setForecast('Raining', 'Ukraine')