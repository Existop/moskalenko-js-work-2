const {Weather} = require('./Weather')

class City{
    /**
     * Describe attributes: name, weather
     */
    constructor(name) {
        this.name = name
        this.weather = new Weather()
    }

    async setForecast (){
        await this.weather.setForecast (this.name)
    }
     async setfindIndex (){
        await this.country.setfindIndex (this.name)
    }
}

module.exports = {City}